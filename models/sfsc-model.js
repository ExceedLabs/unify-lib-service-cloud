const SFSCService = require('../services/sfsc-service');
const RedisService = require('../services/redis-service');
const SalesforceAuth = require('../services/sfsc-auth-service');
const ConversorFieldsService = require('../services/conversor-fields-service');

exports.listCases = async (obj, currentLib) => {
  const self = this;
  const messageService = obj.privateLibraries.MessageService;

  try {
    const credentials = await RedisService.retrieveCredentialsFromRedis(obj);

    const result = await SFSCService.listCases(obj, credentials || currentLib.credentials)
      .catch((error) => {
        console.error('Error on listing cases!');
        console.error(error);

        if (error.response.status === 401) { // request unauthorized: invalid token
          return error.response;
        }
        throw ({ status: error.response.status, error, message: 'error.list.cases' });
      });

    if (result.status >= 200 && result.status <= 304) {
      if (result.data.records.length > 0) { // if there is any Case
        const rawResult = result.data.records[0].Cases.records;
        const { map, data } = ConversorFieldsService.replaceCaseFieldsToCore(obj, rawResult);
        const dataReplaced = obj.privateLibraries.Converters.replaceFields(data, map);

        return messageService.getSuccess(dataReplaced, result.status);
      }
      return messageService.getSuccess([], result.status);
    } if (result.status === 401) {
      const responseNewAccessToken = await SalesforceAuth.getNewAccessToken(currentLib)
        .catch((error) => {
          throw ({ status: error.response.status, error, message: 'error.refresh.token.cases' });
        });

      if (responseNewAccessToken.status >= 200 && responseNewAccessToken.status <= 304) {
        const refreshedCurrentLib = await RedisService
          .saveCredentialsInRedis(obj, currentLib, responseNewAccessToken.data);

        return self.listCases(obj, refreshedCurrentLib);
      }
    } else {
      return messageService.getError([], result.status);
    }
  } catch (error) {
    console.error('Error on listing cases!');
    return messageService.getError(error.stack, 500, error.message);
  }
};

exports.createCase = async (obj, currentLib) => {
  const self = this;
  const messageService = obj.privateLibraries.MessageService;

  try {
    const payload = obj.req.body;
    if (!Object.prototype.hasOwnProperty
      .call(payload, obj.privateLibraries.Converters.fieldsCore.accountId)) return messageService.getError('Missing a correspondent account for the case', 404);

    const credentials = await RedisService.retrieveCredentialsFromRedis(obj);
    const { map, data } = ConversorFieldsService.replaceCoreFieldsToTarget(obj, [payload]);
    const dataReplaced = obj.privateLibraries.Converters.replaceFields(data, map);
    const dataReplacedWithoutNull = ConversorFieldsService.removeAttributesNull(dataReplaced);

    const result = await SFSCService
      .createCase(dataReplacedWithoutNull[0], credentials || currentLib.credentials)
      .catch((error) => {
        console.error('Error on creating case!');
        console.error(error);

        if (error.response.status === 400 // bad request: invalid restricted picklist value
          || error.response.status === 401) { // request unauthorized: invalid token
          return error.response;
        }
        throw ({ status: error.response.status, error, message: 'error.create.case' });
      });

    if (result.status >= 200 && result.status <= 304) {
      const responsePayload = {
        ...payload,
        [obj.privateLibraries.Converters.fieldsCore.caseId]: result.data.id,
      };

      return messageService.getSuccess(responsePayload, result.status);
    } if (result.status === 400) {
      return messageService.getError(result.data, result.status);
    } if (result.status === 401) {
      const responseNewAccessToken = await SalesforceAuth.getNewAccessToken(currentLib)
        .catch((error) => {
          throw ({ status: error.response.status, error, message: 'error.refresh.token.case' });
        });

      if (responseNewAccessToken.status >= 200 && responseNewAccessToken.status <= 304) {
        const refreshedCurrentLib = await RedisService
          .saveCredentialsInRedis(obj, currentLib, responseNewAccessToken.data);

        return self.createCase(obj, refreshedCurrentLib);
      }
    } else {
      return messageService.getError({}, result.status);
    }
  } catch (error) {
    console.error('Error on creating case!');
    return messageService.getError(error.stack, 500, error.message);
  }
};

exports.describeCase = async (obj, currentLib) => {
  const self = this;
  const messageService = obj.privateLibraries.MessageService;

  try {
    const credentials = await RedisService.retrieveCredentialsFromRedis(obj);

    const result = await SFSCService.describeCase(credentials || currentLib.credentials)
      .catch((error) => {
        console.error('Error on describing case!');
        console.error(error);

        if (error.response.status === 401) { // request unauthorized: invalid token
          return error.response;
        }
        throw ({ status: error.response.status, error, message: 'error.describe.case' });
      });

    if (result.status >= 200 && result.status <= 304) {
      return messageService.getSuccess(result.data, result.status);
    } if (result.status === 401) {
      const responseNewAccessToken = await SalesforceAuth.getNewAccessToken(currentLib)
        .catch((error) => {
          throw ({ status: error.response.status, error, message: 'error.refresh.token.case' });
        });

      if (responseNewAccessToken.status >= 200 && responseNewAccessToken.status <= 304) {
        const refreshedCurrentLib = await
        RedisService.saveCredentialsInRedis(obj, currentLib, responseNewAccessToken.data);

        return self.describeCase(obj, refreshedCurrentLib);
      }
    } else {
      return messageService.getError([], result.status);
    }
  } catch (error) {
    console.error('Error on describing case!');
    return messageService.getError(error.stack, 500, error.message);
  }
};

exports.updateCase = async (obj, currentLib) => {
  const self = this;
  const messageService = obj.privateLibraries.MessageService;

  try {
    const { params } = obj.req;
    const payload = obj.req.body;
    const credentials = await RedisService.retrieveCredentialsFromRedis(obj);
    const { map, data } = ConversorFieldsService.replaceCoreFieldsToTarget(obj, [payload]);
    const dataReplaced = obj.privateLibraries.Converters.replaceFields(data, map);
    const dataReplacedWithoutNull = ConversorFieldsService.removeAttributesNull(dataReplaced);

    const result = await SFSCService
      .updateCase(params, dataReplacedWithoutNull[0], credentials || currentLib.credentials)
      .catch((error) => {
        console.error('Error on updating case!');
        console.error(error);

        if (error.response.status === 400 // bad request: invalid restricted picklist value
          || error.response.status === 401 // request unauthorized: invalid token
          || error.response.status === 404) { // bad request: wrong case ID
          return error.response;
        }
        throw ({ status: error.response.status, error, message: 'error.update.case' });
      });

    if (result.status >= 200 && result.status <= 304) {
      const responsePayload = {
        ...payload,
        [obj.privateLibraries.Converters.fieldsCore.caseId]: params.caseId,
      };

      return messageService.getSuccess(responsePayload, result.status);
    } if (result.status === 400) {
      return messageService.getError(result.data, result.status);
    } if (result.status === 401) {
      const responseNewAccessToken = await SalesforceAuth.getNewAccessToken(currentLib)
        .catch((error) => {
          throw ({ status: error.response.status, error, message: 'error.refresh.token.case' });
        });

      if (responseNewAccessToken.status >= 200 && responseNewAccessToken.status <= 304) {
        const refreshedCurrentLib = await
        RedisService.saveCredentialsInRedis(obj, currentLib, responseNewAccessToken.data);

        return self.updateCase(obj, refreshedCurrentLib);
      }
    } if (result.status === 404) {
      return messageService.getError(result.data, result.status);
    }
    return messageService.getError({}, result.status);
  } catch (error) {
    console.error('Error on updating case!');
    return messageService.getError(error.stack, 500, error.message);
  }
};

exports.deleteCase = async (obj, currentLib) => {
  const self = this;
  const messageService = obj.privateLibraries.MessageService;

  try {
    const { params } = obj.req;
    const credentials = await RedisService.retrieveCredentialsFromRedis(obj);

    const result = await SFSCService.deleteCase(params, credentials || currentLib.credentials)
      .catch((error) => {
        console.error('Error on deleting case!');
        console.error(error);

        if (error.response.status === 401 // request unauthorized: invalid token
          || error.response.status === 404) { // bad request: wrong case ID
          return error.response;
        }
        throw ({ status: error.response.status, error, message: 'error.delete.case' });
      });

    if (result.status >= 200 && result.status <= 304) {
      const responsePayload = {
        [obj.privateLibraries.Converters.fieldsCore.caseId]: params.caseId,
      };

      return messageService.getSuccess(responsePayload, result.status);
    } if (result.status === 401) {
      const responseNewAccessToken = await SalesforceAuth.getNewAccessToken(currentLib)
        .catch((error) => {
          throw ({ status: error.response.status, error, message: 'error.refresh.token.case' });
        });

      if (responseNewAccessToken.status >= 200 && responseNewAccessToken.status <= 304) {
        const refreshedCurrentLib = await
        RedisService.saveCredentialsInRedis(obj, currentLib, responseNewAccessToken.data);

        return self.deleteCase(obj, refreshedCurrentLib);
      }
    } if (result.status === 404) {
      return messageService.getError(result.data, result.status);
    }
    return messageService.getError({}, result.status);
  } catch (error) {
    console.error('Error on deleting case!');
    return messageService.getError(error.stack, 500, error.message);
  }
};
