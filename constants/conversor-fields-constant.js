module.exports = {
  case: {
    id: 'Id',
    account_id: 'AccountId',
    owner_id: 'OwnerId',
    status: 'Status',
    case_number: 'CaseNumber',
    priority: 'Priority',
    type: 'Type',
    origin: 'Origin',
    reason: 'Reason',
    currency_iso_code: 'CurrencyIsoCode',
    created_date: 'CreatedDate',
    last_modified_by_id: 'LastModifiedById',
    subject: 'Subject',
    description: 'Description',
    comments: 'Comments',
  },
};
