

module.exports = {
  OAUTH: {
    HOST_BASE_URL: 'https://login.salesforce.com/services/oauth2/',
  },
  REST_API: {
    VERSION: 'v43.0',
  },
};
