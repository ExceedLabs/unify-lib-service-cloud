require('./load-env');
const Chai = require('chai');
const Util = require('./util');
const ConversorFieldsService = require('../../services/conversor-fields-service');
const CoreCaseStandard = require('../unit/resources/core/case');

const should = Chai.should();

describe('## FIELDS REPLACEMENT SERVICE ##', () => {
  it('#01 mapping case fields to core fields', () => {
    const data = [
      {
        Status: 'New', Origin: 'Phone', Subject: 'UNIFY SUBJECT TEST', CurrencyIsoCode: 'CAD',
      },
    ];

    const obj = {
      libObjId: 'case',
      privateLibraries: {
        Converters: {
          fieldsCore: {
            ...CoreCaseStandard,
          },
        },
      },
    };

    const result = ConversorFieldsService.replaceCaseFieldsToCore(obj, data);
    result.map.list.should.equal('case');
    result.map.item.status.should.equal('Status');
    result.map.item.caseOrigin.should.equal('Origin');
    result.map.item.subject.should.equal('Subject');
    result.map.item.caseCurrency.should.equal('CurrencyIsoCode');
  });

  it('#02 replacing case fields to core fields', () => {
    const caseData = [
      {
        Status: 'New', Origin: 'Phone', Subject: 'UNIFY SUBJECT TEST', CurrencyIsoCode: 'CAD',
      },
    ];

    const obj = {
      libObjId: 'case',
      privateLibraries: {
        Converters: {
          fieldsCore: {
            ...CoreCaseStandard,
          },
          replaceFields: Util.replaceFields,
        },
      },
    };

    const { map, data } = ConversorFieldsService.replaceCaseFieldsToCore(obj, caseData);
    const dataReplaced = obj.privateLibraries.Converters.replaceFields(data, map);
    dataReplaced[0].status.should.equal('New');
    dataReplaced[0].caseOrigin.should.equal('Phone');
    dataReplaced[0].subject.should.equal('UNIFY SUBJECT TEST');
    dataReplaced[0].caseCurrency.should.equal('CAD');
  });

  it('#03 removing null-valued fields on Case', () => {
    const caseData = [
      {
        Status: 'New', Origin: 'Phone', Subject: 'UNIFY SUBJECT TEST', CurrencyIsoCode: 'CAD',
      },
    ];

    const obj = {
      libObjId: 'case',
      privateLibraries: {
        Converters: {
          fieldsCore: {
            ...CoreCaseStandard,
          },
          replaceFields: Util.replaceFields,
        },
      },
    };

    const { map, data } = ConversorFieldsService.replaceCaseFieldsToCore(obj, caseData);
    const dataReplaced = obj.privateLibraries.Converters.replaceFields(data, map);
    const dataReplacedLength = Object.keys(dataReplaced[0]).length;
    const dataReplacedCleaned = ConversorFieldsService.removeAttributesNull(dataReplaced);
    const dataReplacedCleanedLength = Object.keys(dataReplacedCleaned[0]).length;
    const condition = (dataReplacedCleanedLength < dataReplacedLength);
    condition.should.equal(true);
  });
});
