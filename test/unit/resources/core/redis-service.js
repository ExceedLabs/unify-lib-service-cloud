const redis = require('redis');

class RedisService {
  constructor() {
    try {
      this.cache = redis.createClient({
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
        password: process.env.REDIS_PASSWORD,
      });
    } catch (err) {
      return {
        status: 500,
        message: 'Error to connect to Redis',
        error: err,
      };
    }
  }

  /**
   * @param {string} key - [required]
  */
  get(key) {
    return new Promise((resolve, reject) => {
      this.cache.get(key, (err, result) => {
        if (err) {
          return reject({
            status: 500,
            message: 'Error on trying to get data from Redis',
            error: err
          });
        }
        resolve({
          status: 200,
          data: result,
          message: 'Successfully',
        });
      });
    });
  }

  /**
   * @param {string} key          - [required]
   * @param {object} data         - [required]
   * @param {string} expireTime   - [optional] EX means "seconds"
   * @param {number} expireNumber - [optional] 780, 200, 60
   * 780 seconds represents 13min
  */
  set(key, data, expireTime, expireNumber) {
    return new Promise((resolve, reject) => {
      this.cache.set(key, JSON.stringify(data), expireTime = 'EX', expireNumber = 780, (err, result) => {
        if (err) {
          return reject({
            status: 500,
            error: `An error occurred on trying to set data into Redis.`,
            details: err,
          });
        }
        resolve(result);
      });
    });
  }
}

module.exports = new RedisService();
