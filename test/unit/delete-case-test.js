require('./load-env');
const Chai = require('chai');
const Util = require('./util');
const redisService = require('../unit/resources/core/redis-service');
const messageService = require('../unit/resources/core/message-status-service');
const SFSCModel = require('../../models/sfsc-model');
const CoreCaseStandard = require('../unit/resources/core/case');

const should = Chai.should();

describe('## DELETE A CASE RESOURCE ##', () => {
  const expiredAccessToken = '00D6A000002i94T!AQIAQCHHtby2eQYa2tFiQM7umv8HDZ1uqV8A_nItRHvHhkuw_3KikZhs0FsRG2YV4ECKvIgb90spZ_XOmnKex32R7m_h4TDH';
  const newCaseBody = {
    accountId: '0016A00000RSvVvQAL',
    status: 'New',
    caseOrigin: 'Phone',
    subject: `SUBJECT - UNIFY TEST AT ${new Date().toISOString()}`,
    caseCurrency: 'CAD',
  };
  const currentLib = {
    credentials: {
      access_token: expiredAccessToken,
      refresh_token: '5Aep861w1PjQhFtnoBewhy1oLLyuPuDf1db1cD65Hsp6XVYEyU6pRQKluUYGj3ISpOPWuDoG7kQLYbmsmm8WEVS',
      signature: 'jukd4sFr1vkK7fScg4CFeWNR6iM80fNNmPKg0JA+7X8=',
      scope: 'refresh_token web api id full',
      instance_url: 'https://unifybigdata-dev-ed.my.salesforce.com',
      id: 'https://login.salesforce.com/id/00D6A000002i94TUAQ/0056A00000224rEQAQ',
      token_type: 'Bearer',
      issued_at: '1543864790671',
      version: 'v43.0',
      consumerKey: '3MVG9CEn_O3jvv0xfdlIbQtpdktiYlCsOXoyJF0chEkj3zatRXWOjP2FmzIjsuyETygci01E6plJ7WQ63bGyE',
      consumerSecret: '7556587165188411505',
    },
  };
  let obj = {
    infoOrganization: {
      accountId: 'admin@osf-beauty-test.com',
    },
    privateLibraries: {
      MessageService: messageService,
      RedisService: redisService,
      Converters: {
        fieldsCore: {
          ...CoreCaseStandard,
        },
        replaceFields: Util.replaceFields,
      },
    },
    req: {
      params: {
        personEmail: 'breno.test@osf-test-ct-01.com',
        caseId: '',
      },
      body: {
        ...newCaseBody,
      },
    },
    libObjId: 'case',
  };

  it('#01 deleting a case', (done) => {
    SFSCModel.createCase(obj, currentLib).then((successCreate) => {
      const conditionCreate = (successCreate.message === 'success');
      conditionCreate.should.equal(true);
      const { caseId } = successCreate.data;
      obj.req.params.caseId = caseId;

      SFSCModel.deleteCase(obj, currentLib).then((successDelete) => {
        const conditionDelete = (successDelete.message === 'success');
        conditionDelete.should.equal(true);
        done();
      });
    });
  });

  it('#02 deleting a non existent case', (done) => {
    obj.req.params.caseId = 'thisIsANonExistentCase';

    SFSCModel.deleteCase(obj, currentLib).then((successDelete) => {
      const conditionDelete = (successDelete.message === 'error');
      conditionDelete.should.equal(true);
      done();
    });
  });
});
