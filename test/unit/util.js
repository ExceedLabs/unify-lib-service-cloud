const { DataTransform } = require('node-json-transform');

exports.replaceFields = (data, map) => {
  const dataTransform = DataTransform(data, map);
  const result = dataTransform.transform();
  return result;
};
