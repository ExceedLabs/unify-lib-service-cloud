const Axios = require('axios');
const SFSCConstants = require('../constants/sfsc-constants.js');

exports.getNewAccessToken = async (currentLib) => {
  const refreshToken = currentLib.credentials.refresh_token;
  const { consumerKey } = currentLib.credentials;
  const { consumerSecret } = currentLib.credentials;
  const HOST = `${SFSCConstants.OAUTH.HOST_BASE_URL}token?grant_type=refresh_token&client_id=${consumerKey}&client_secret=${consumerSecret}&refresh_token=${refreshToken}`;

  return Axios({
    method: 'get',
    baseURL: HOST,
  });
};
