const _ = require('lodash');
const CoreFieldsDefault = require('./converters/core-fields-default');

/**
 * Used to get the data from SFSC and return to Clienteling
 */
exports.replaceCaseFieldsToCore = (obj, caseData) => {
  try {
    const fieldsCore = obj.privateLibraries.Converters.fieldsCore;
    const libObjId = obj.libObjId;

    const map = {
      list: libObjId,
      item: {
        ...CoreFieldsDefault.getCoreFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: caseData,
    };

    return {
      map,
      data,
    };
  } catch (error) {
    throw ({
      status: 500, error, message: 'error.replace.case.fields', service: 'conversor-fields-service.js',
    });
  }
};

/**
 * Used to create or update a case
 */
exports.replaceCoreFieldsToTarget = (obj, caseData) => {
  try {
    const fieldsCore = obj.privateLibraries.Converters.fieldsCore;
    const libObjId = obj.libObjId;

    const map = {
      list: libObjId,
      item: {
        ...CoreFieldsDefault.getCaseFields(fieldsCore),
      },
    };

    const data = {
      [libObjId]: caseData,
    };

    return {
      map,
      data,
    };
  } catch (error) {
    throw ({
      status: 500, error, message: 'error.replace.case.fields', service: 'conversor-fields-service.js',
    });
  }
};

exports.removeAttributesNull = (allData) => {
  try {
    return _.map(allData, (data) => {
      let newObj = {};
      for (const key in data) {
        if (data[key]) {
          newObj = { ...newObj, ...{ [key]: data[key] } };
        }
      }
      return newObj;
    });
  } catch (error) {
    return {};
  }
};
