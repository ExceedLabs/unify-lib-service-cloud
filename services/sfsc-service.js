const Axios = require('axios');
const SFSCConstants = require('../constants/sfsc-constants.js');

exports.listCases = async (obj, credentials) => {
  const baseUrl = credentials.instance_url;
  const tokenType = credentials.token_type;
  const accessToken = credentials.access_token;
  const apiVersion = SFSCConstants.REST_API.VERSION;
  const { personEmail } = obj.req.params;

  return Axios({
    method: 'get',
    url: `${baseUrl}/services/data/${apiVersion}/query`,
    headers: { Authorization: `${tokenType} ${accessToken}` },
    params: { q: decodeURIComponent(`SELECT Id, (SELECT Id, OwnerId, Status, CaseNumber, Priority, Type, Origin, Reason, CurrencyIsoCode, CreatedDate, Product__c, LastModifiedById, Subject, Description, Comments FROM Cases) FROM Account WHERE PersonEmail = '${personEmail}'`) },
  });
};

exports.describeCase = async (credentials) => {
  const baseUrl = credentials.instance_url;
  const tokenType = credentials.token_type;
  const accessToken = credentials.access_token;
  const apiVersion = SFSCConstants.REST_API.VERSION;

  return Axios({
    method: 'get',
    url: `${baseUrl}/services/data/${apiVersion}/sobjects/case/describe`,
    headers: { Authorization: `${tokenType} ${accessToken}` },
  });
};

exports.createCase = async (payload, credentials) => {
  const baseUrl = credentials.instance_url;
  const tokenType = credentials.token_type;
  const accessToken = credentials.access_token;
  const apiVersion = SFSCConstants.REST_API.VERSION;

  return Axios({
    method: 'post',
    url: `${baseUrl}/services/data/${apiVersion}/sobjects/case`,
    headers: { Authorization: `${tokenType} ${accessToken}` },
    data: payload,
  });
};

exports.updateCase = async (params, payload, credentials) => {
  const baseUrl = credentials.instance_url;
  const tokenType = credentials.token_type;
  const accessToken = credentials.access_token;
  const apiVersion = SFSCConstants.REST_API.VERSION;
  const { caseId } = params;

  return Axios({
    method: 'patch',
    url: `${baseUrl}/services/data/${apiVersion}/sobjects/case/${caseId}`,
    headers: { Authorization: `${tokenType} ${accessToken}` },
    data: payload,
  });
};

exports.deleteCase = async (params, credentials) => {
  const baseUrl = credentials.instance_url;
  const tokenType = credentials.token_type;
  const accessToken = credentials.access_token;
  const apiVersion = SFSCConstants.REST_API.VERSION;
  const { caseId } = params;

  return Axios({
    method: 'delete',
    url: `${baseUrl}/services/data/${apiVersion}/sobjects/case/${caseId}`,
    headers: { Authorization: `${tokenType} ${accessToken}` },
  });
};
