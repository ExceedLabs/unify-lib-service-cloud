const CaseFields = require('./case-default');

exports.getCoreFields = (fieldsCore) => {
  try {
    return CaseFields.getCoreFields(fieldsCore);
  } catch (error) {
    throw (error);
  }
};

exports.getCaseFields = (fieldsCore) => {
  try {
    return CaseFields.getCaseFields(fieldsCore);
  } catch (error) {
    throw (error);
  }
};
