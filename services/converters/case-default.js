const FieldsConstant = require('../../constants/conversor-fields-constant');

const FIELD_CASE = FieldsConstant.case;

exports.getCoreFields = fieldsCore => ({
  [fieldsCore.caseId]: FIELD_CASE.id,
  [fieldsCore.accountId]: FIELD_CASE.account_id,
  [fieldsCore.caseOwner]: FIELD_CASE.owner_id,
  [fieldsCore.status]: FIELD_CASE.status,
  [fieldsCore.caseNumber]: FIELD_CASE.case_number,
  [fieldsCore.priority]: FIELD_CASE.priority,
  [fieldsCore.type]: FIELD_CASE.type,
  [fieldsCore.caseOrigin]: FIELD_CASE.origin,
  [fieldsCore.caseReason]: FIELD_CASE.reason,
  [fieldsCore.caseCurrency]: FIELD_CASE.currency_iso_code,
  [fieldsCore.dateOpened]: FIELD_CASE.created_date,
  [fieldsCore.lastModifiedBy]: FIELD_CASE.last_modified_by_id,
  [fieldsCore.subject]: FIELD_CASE.subject,
  [fieldsCore.description]: FIELD_CASE.description,
  [fieldsCore.internalComments]: FIELD_CASE.comments,
});

exports.getCaseFields = fieldsCore => ({
  [FIELD_CASE.id]: fieldsCore.caseId,
  [FIELD_CASE.account_id]: fieldsCore.accountId,
  [FIELD_CASE.owner_id]: fieldsCore.caseOwner,
  [FIELD_CASE.status]: fieldsCore.status,
  [FIELD_CASE.case_number]: fieldsCore.caseNumber,
  [FIELD_CASE.priority]: fieldsCore.priority,
  [FIELD_CASE.type]: fieldsCore.type,
  [FIELD_CASE.origin]: fieldsCore.caseOrigin,
  [FIELD_CASE.reason]: fieldsCore.caseReason,
  [FIELD_CASE.currency_iso_code]: fieldsCore.caseCurrency,
  [FIELD_CASE.created_date]: fieldsCore.dateOpened,
  [FIELD_CASE.last_modified_by_id]: fieldsCore.lastModifiedBy,
  [FIELD_CASE.subject]: fieldsCore.subject,
  [FIELD_CASE.description]: fieldsCore.description,
  [FIELD_CASE.comments]: fieldsCore.internalComments,
});
