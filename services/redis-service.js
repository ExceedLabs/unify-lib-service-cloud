getRedisInstance = (obj) => {
  return obj.privateLibraries.RedisService;
};

exports.saveCredentialsInRedis = async(obj, currentLib, newCredentials) => {
  const redis = getRedisInstance(obj);

  currentLib.credentials.access_token = newCredentials.access_token;
  currentLib.credentials.id = newCredentials.id;
  currentLib.credentials.instance_url = newCredentials.instance_url;
  currentLib.credentials.issued_at = newCredentials.issued_at;
  currentLib.credentials.scope = newCredentials.scope;
  currentLib.credentials.signature = newCredentials.signature;
  currentLib.credentials.token_type = newCredentials.token_type;

  await redis.set(`${obj.infoOrganization.accountId}_credentials`, currentLib.credentials, 'EX', 720)
    .catch(error => {
     // TODO: handle error (try to update again?)
      console.error('Failed to save credentials in Redis DB.');
      console.error(error);
    });

    return currentLib;
};

exports.retrieveCredentialsFromRedis = async (obj) => {
  try {
    const redis = getRedisInstance(obj);

    const result = await redis.get(`${obj.infoOrganization.accountId}_credentials`)
      .catch(error => error);

    return JSON.parse(result.data);
  } catch (error) {
    console.error('Failed to retrieve credentials from Redis DB.');
    return null;
  }
};
