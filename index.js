const SFSCModel = require('./models/sfsc-model');

// CASES
exports.createCase = SFSCModel.createCase;
exports.describeCase = SFSCModel.describeCase;
exports.deleteCase = SFSCModel.deleteCase;
exports.listCases = SFSCModel.listCases;
exports.updateCase = SFSCModel.updateCase;
